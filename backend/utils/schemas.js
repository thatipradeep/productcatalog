import mongoose from "mongoose";
/**
 * creating a  schema to be used to convert into a model for product data
 * for the MongoDB implementation.
 * schema for product data
 */
const product_schema = mongoose.Schema({
  product_name:        { type: String, required: true },
  product_description: { type: String, required: true },
  product_price:       { type: String, required: true },
  discount:            { type: String, required: true },
  merchant_name:       { type: String, required: true },
  quantity:            { type: String, required: true },
});



export default product_schema;
